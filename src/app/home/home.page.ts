import { Component } from '@angular/core';
import { IonRouterOutlet, Platform, ToastController } from '@ionic/angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Plugins } from '@capacitor/core';
const { App } = Plugins;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  databaseObj: SQLiteObject;
  readonly databaseName: string = 'test.db';
  readonly tableName: string = 'test';

  nameModel = '';
  rowData: any = [];

  // Handle Update Row Operation
  updateActive: boolean;
  toUpdateItem: any;

  constructor(
    private platform: Platform,
    private sqlite: SQLite,
    private routerOutlet: IonRouterOutlet,
    private toastController: ToastController
  ) {
    this.platform.ready().then(() => {
      this.createDB();
    }).catch(error => {
      console.log(error);
    });
    this.platform.backButton.subscribeWithPriority(-1, () => {
      if (!this.routerOutlet.canGoBack()) {
        App.exitApp();
      }
    });
  }

  // Create DB if not there
  createDB() {
    this.sqlite.create({
      name: this.databaseName,
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        this.databaseObj = db;
        this.showToast('test Database Created.');
      })
      .catch(e => {
        this.showToast('error: ' + JSON.stringify(e));
      });
  }

  // Create table
  createTable() {
    this.databaseObj.executeSql(`
      CREATE TABLE IF NOT EXISTS ${this.tableName} (pid INTEGER PRIMARY KEY, Name VARCHAR(255))
    `, [])
      .then(() => {
        this.showToast('Table Created!');
      })
      .catch(e => {
        this.showToast('error: ' + JSON.stringify(e));
      });
  }

  // Insert row in the table
  insertRow() {
    // Value shouldn't be empty
    if (!this.nameModel.length) {
      this.showToast('Enter Name');
      return;
    }

    this.databaseObj.executeSql(`
      INSERT INTO ${this.tableName} (Name) VALUES ('${this.nameModel}')
    `, [])
      .then(() => {
        this.showToast('Row Inserted!');
        this.getRows();
      })
      .catch(e => {
        this.showToast('error: ' + JSON.stringify(e));
      });
  }

  // Retrieve rows from table
  getRows() {
    this.databaseObj.executeSql(`
      SELECT * FROM ${this.tableName}
    `, [])
    .then((res) => {
      this.rowData = [];
      if (res.rows.length > 0) {
        for (let i = 0; i < res.rows.length; i++) {
          this.rowData.push(res.rows.item(i));
        }
      }
    })
    .catch(e => {
      this.showToast('error: ' + JSON.stringify(e));
    });
  }

  // Delete single row
  deleteRow(item) {
    this.databaseObj.executeSql(`
      DELETE FROM ${this.tableName} WHERE pid = ${item.pid}
    `, [])
      .then(() => {
        this.showToast('Row Deleted!');
        this.getRows();
      })
      .catch(e => {
        this.showToast('error: ' + JSON.stringify(e));
      });
  }

  // Enable update mode and keep row data in a variable
  enableUpdate(item) {
    this.updateActive = true;
    this.toUpdateItem = item;
    this.nameModel = item.name;
  }

  // Update row with saved row id
  updateRow() {
    this.databaseObj.executeSql(`
      UPDATE ${this.tableName}
      SET Name = '${this.nameModel}'
      WHERE pid = ${this.toUpdateItem.pid}
    `, [])
      .then(() => {
        this.showToast('Row Updated!');
        this.updateActive = false;
        this.getRows();
      })
      .catch(e => {
        this.showToast('error: ' + JSON.stringify(e));
      });
  }

  // Show toast
  async showToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 1500,
      position: 'bottom'
    });
    await toast.present();
  }
}
